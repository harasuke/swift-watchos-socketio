//
//  ProgramViewModel.swift
//  socketio_example
//
//  Created by Mirko Spinato on 05/07/22.
//

import UIKit

final class ProgramViewModel: ObservableObject {
    private(set) var connectivityProvider: WKConnectivityProvider
    
    init(connectivityProvider: WKConnectivityProvider) {
        self.connectivityProvider = connectivityProvider
    }
}
