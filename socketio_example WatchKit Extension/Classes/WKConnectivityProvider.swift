//
//  WKConnectivityProvider.swift
//  socketio_example WatchKit Extension
//
//  Created by Mirko Spinato on 05/07/22.
//

import UIKit
import WatchConnectivity
import CoreData

class WKConnectivityProvider: NSObject, WCSessionDelegate, ObservableObject {
    
    private let session: WCSession = WCSession.default
    @Published var isReachable = false
    
    @Published var jsonData:Result = Result()
    @Published var greet:String = ""
    
    init(session:WCSession = .default) {
            super.init()
            self.session.delegate = self
        if WCSession.isSupported() {
            self.session.activate()
            print("WKConnectivityProvider started on Watch")
        }
    }
    
    func send(msg: [String: Any]) -> Void {
        session.sendMessage(msg, replyHandler: nil) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        DispatchQueue.main.async {
            self.isReachable = session.isReachable
            print("WatchOS should be reachable")
        }
    }
    
    //MARK: Message received from the iPhone
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            
            switch message.first?.key {
                case "jsonData":
                    print("json received")
                    NSKeyedUnarchiver.setClass(Result.self, forClassName: "JSONResponse")
                    let unarchivedMessage = try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [Result.self, NSString.self, NSNumber.self], from: message["jsonData"] as! Data)
                        
                    self.jsonData = unarchivedMessage! as! Result
                    break
                
                case "greetingsResponse":
                    NSKeyedUnarchiver.setClass(NSString.self, forClassName: "greetingsResponse")
                    let unarchivedMessage = try! NSKeyedUnarchiver.unarchivedObject(ofClasses: [Result.self, NSString.self, NSNumber.self], from: message["greetingsResponse"] as! Data)
                        
                    self.greet = unarchivedMessage! as! String
                    break
                
                default: break
                    
            }
            
        }
    }
    
    func sendJSONRequest(_ messageText: String) {
        if session.isReachable {
                
                NSKeyedArchiver.setClassName("JSONRequest", for: NSString.self)
                let encoded = try! NSKeyedArchiver.archivedData(withRootObject: messageText, requiringSecureCoding: true)
                self.session.sendMessage(["url": encoded], replyHandler: nil)

        }
    }
    
    func sendForSocket(_ messageText:String) {
        if session.isReachable {
            
            NSKeyedArchiver.setClassName("socketGreetings", for: NSString.self)
            let encoded = try! NSKeyedArchiver.archivedData(withRootObject: messageText, requiringSecureCoding: true)
            self.session.sendMessage(["socketGreetings": encoded], replyHandler: nil)
            
        }
    }

}

