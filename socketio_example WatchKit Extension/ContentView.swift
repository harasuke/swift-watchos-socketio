//
//  ContentView.swift
//  socketio_example WatchKit Extension
//
//  Created by Mirko Spinato on 05/07/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
