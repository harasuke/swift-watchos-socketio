//
//  MainView.swift
//  socketio_example WatchKit Extension
//
//  Created by Mirko Spinato on 05/07/22.
//

import SwiftUI
import CoreData

struct MainView: View {

    @StateObject var connect = WKConnectivityProvider()
    
    @FetchRequest(sortDescriptors: []) private var oldMsg: FetchedResults<Messages>
//    @FetchRequest(fetchRequest: getOldMessages()) var oldMsg: FetchedResults<Messages>
//    static func getOldMessages() -> NSFetchRequest<Messages> {
//        let request:NSFetchRequest<Messages> = Messages.fetchRequest()
////        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: false)
////        request.sortDescriptors = [sortDescriptor]
//        return request
//    }
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    Button(action: {
                        connect.sendJSONRequest("https://jsonplaceholder.typicode.com/todos/1")
                    }, label: {Text("Send message")} )
                    Text(connect.jsonData.title)
                    
                    Button(action: {
                        connect.sendForSocket("hello from watch")
                    }, label: {Text("Send message")} )
                    Text(connect.greet)
                }
            }
        }.edgesIgnoringSafeArea(.bottom)
        
    }
}

struct MainView_Previews: PreviewProvider {
    private static var txt:WKConnectivityProvider = WKConnectivityProvider()
    static var previews: some View {
        MainView()
    }
}
