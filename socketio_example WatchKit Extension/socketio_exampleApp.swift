//
//  socketio_exampleApp.swift
//  socketio_example WatchKit Extension
//
//  Created by Mirko Spinato on 05/07/22.
//

import SwiftUI

@main
struct socketio_exampleApp: App {
    
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                MainView()
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
            }
        }
    }
}
