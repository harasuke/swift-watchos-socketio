//
//  NetworkProvider.swift
//  socketio_example
//
//  Created by Mirko Spinato on 05/07/22.
//

import UIKit

struct SuccessInfo: Decodable {
    let success: String
}

class NetworkProvider {
    
    static func initRequest(urlString:String) {
        Task {
            guard let url = URL(string: urlString) else { fatalError("Missing URL") }
            var urlRequest = URLRequest(url: url)
            urlRequest.addValue("text/html", forHTTPHeaderField: "Content-Type")
            
            let (data, response) = try await URLSession.shared.data(for: urlRequest)
            guard (response as? HTTPURLResponse)?.statusCode == 200 else { fatalError("Error while fetching data") }
            
            print(String(data: data, encoding: .utf8) ?? "")
//            let successInfo = try JSONDecoder().decode(SuccessInfo.self, from: data)
            
//            print(String(data: data, encoding: .utf8) ?? "default value")
//            print("Success: \(successInfo.success)")
        }
    }
}


