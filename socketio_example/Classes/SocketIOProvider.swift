//
//  SocketIOProvider.swift
//  socketio_example
//
//  Created by Mirko Spinato on 05/07/22.
//

import Foundation
import SocketIO

class Socket: ObservableObject {
    static private let manager: SocketManager = SocketManager(socketURL: URL(string: "TYPE YOUR SERVER URL HERE")!, config: [.log(true), .compress])
    
    private var connect = WKConnectivityProvider()
    
    @Published var messages:[String] = [String]()
    
    init() {
        let socket = Socket.manager.defaultSocket
        socket.on(clientEvent: .connect) { (data, ack) in
            print("Connected")
        }
        
        socket.on("greet", callback: { [weak self] (data, ack) in
            //data is always an array of dictionary ?!
            if let data = data[0] as? [String: String],
               let rawData = data["key"] {
                DispatchQueue.main.async {
                    self?.messages.append(rawData)
                    self?.connect.sendGreetingsToWatch(msg: rawData)
                    
                }
            }
            
        })
        
        socket.connect()
    }
    
    static func sendGreetings(msg:String) {
        Socket.manager.defaultSocket.emit("greetings", msg)
    }
}
