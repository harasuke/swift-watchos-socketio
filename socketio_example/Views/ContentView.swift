//
//  ContentView.swift
//  socketio_example
//
//  Created by Mirko Spinato on 05/07/22.
//

import SwiftUI

struct ContentView: View {

    @StateObject var connect = WKConnectivityProvider()
    @ObservedObject var socket = Socket()
    
    var body: some View {
        VStack {
            Button(action: {
                NetworkProvider.initRequest(urlString: "http://harasuke.duckdns.org:3091/api/test")
            }, label: {
                Text("Call API")
            })
            List(socket.messages, id: \.self) { m in
                Text(m)
            }
            
            Text(connect.incomingMessage)
            List(connect.oldMessages, id:\.self) { msg in
                Text(msg)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
