//
//  socketio_exampleApp.swift
//  socketio_example
//
//  Created by Mirko Spinato on 05/07/22.
//

import SwiftUI

@main
struct socketio_exampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
